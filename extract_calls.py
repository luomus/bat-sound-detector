from detector import Detector
from config import DetectorConfig
import os, sys, getopt
from functools import partial
from multiprocessing import Pool
from utils import wavfile

def extract_calls(input_folder, output_folder, file_path):
    input_path = os.path.join(input_folder, file_path)

    head, tail = os.path.split(file_path)
    path = os.path.join(output_folder, head)
    os.makedirs(path, exist_ok=True)

    ranges = Detector(DetectorConfig()).get_detections(input_path)

    for i in range(len(ranges)):
        data_range = ranges[i]
        fs, data = wavfile.read(input_path, data_range[0], data_range[1])
        result_file_name = '{}_{}.WAV'.format(tail[:-4], i)

        wavfile.write(os.path.join(path, result_file_name), fs, data)

if __name__ == '__main__':
    if (len(sys.argv)) < 3:
        print('usage: extract_calls.py <inputfolder> <outputfolder>')
        sys.exit(2)

    input_folder = sys.argv[1]
    output_folder = sys.argv[2]

    input_files = []
    for path, subdirs, files in os.walk(input_folder):
        for name in files:
            relpath = os.path.relpath(path, input_folder)
            input_files.append(os.path.join(relpath, name))

    p = Pool(1)
    func = partial(extract_calls, input_folder, output_folder)
    p.map(func, input_files)
