import numpy as np

class DetectorConfig:
    # long audio files are processed in parts
    part_max_length = 6 * 60 # in seconds
    part_overlap = 5 # in seconds

    # spectrogram parameters
    window_size = 0.02 # in seconds
    overlap = 0.75 # in percents

    # noise reduction parameters
    remove_noise = True
    nr_sensitivity = 2
    nr_decrease = 0.3
    nr_smoothing_time = 1
    nr_smoothing_frequency = 2

    # median clipping parameters
    mc_segment_length = 1.5
    mc_threshold = 3
    mc_use_row_mask = False
    remove_clock_noise = True
    clock_interval = 0.085

    # broadband noise reduction parameters
    remove_broadband_noise = True
    min_frequency = 20000
    max_frequency = 80000
    min_certain_frequency = 16000

    # minumum thresholds for connected components
    cc_width_threshold = 0.01 # in seconds
    cc_height_threshold = 500 # in hertz
    cc_area_theshold = 2

    # thresholds that determine whether two components belong to same group
    nc_x_threshold = 0.5 # in seconds
    nc_x_threshold_wide = 2.5 # in seconds
    nc_y_threshold_bottom = 2500 # in hertz
    nc_y_threshold_top = 10000 # in hertz

    min_distance_between_pulses = 0.035 # in seconds
    min_number_of_calls = 3
    padding = 1

class DetectorConfigState:
    def __init__(self, detector_config, fs, data_length):
        self.fs = fs
        part_max_length = int(detector_config.part_max_length * fs)
        self.part_overlap = int(detector_config.part_overlap * fs)
        self.number_of_parts = int(np.ceil((data_length - part_max_length) / (part_max_length - self.part_overlap)) + 1)
        self.part_length = int(np.ceil((data_length + self.part_overlap * (self.number_of_parts - 1)) / self.number_of_parts))

        self.nperseg = int(detector_config.window_size * fs)
        self.noverlap = int(self.nperseg * detector_config.overlap)

        self.remove_noise = detector_config.remove_noise
        self.nr_sensitivity = detector_config.nr_sensitivity
        self.nr_decrease = detector_config.nr_decrease
        self.nr_smoothing_time = detector_config.nr_smoothing_time
        self.nr_smoothing_frequency = detector_config.nr_smoothing_frequency

        self.mc_segment_length = self._convert_time(detector_config.mc_segment_length)
        self.mc_threshold = detector_config.mc_threshold
        self.mc_use_row_mask = detector_config.mc_use_row_mask
        self.remove_clock_noise = detector_config.remove_clock_noise
        self.clock_interval = self._convert_time(detector_config.clock_interval)

        self.remove_broadband_noise = detector_config.remove_broadband_noise
        self.min_frequency = detector_config.min_frequency
        self.max_frequency = detector_config.max_frequency
        self.min_certain_frequency = detector_config.min_certain_frequency

        self.cc_width_threshold = self._convert_time(detector_config.cc_width_threshold)
        self.cc_height_threshold = self._convert_frequency(detector_config.cc_height_threshold)
        self.cc_area_theshold = self.cc_width_threshold * self.cc_height_threshold * detector_config.cc_area_theshold

        self.nc_x_threshold = self._convert_time(detector_config.nc_x_threshold)
        self.nc_x_threshold_wide = self._convert_time(detector_config.nc_x_threshold_wide)
        self.nc_y_threshold_bottom = self._convert_frequency(detector_config.nc_y_threshold_bottom)
        self.nc_y_threshold_top = self._convert_frequency(detector_config.nc_y_threshold_top)

        self.min_distance_between_pulses = self._convert_time(detector_config.min_distance_between_pulses)
        self.min_number_of_calls = detector_config.min_number_of_calls
        self.padding = self._convert_time(detector_config.padding)

    def _convert_time(self, time):
        return round(time / ((self.nperseg - self.noverlap) / self.fs))

    def _convert_frequency(self, freq):
        return round(freq / self.fs * self.nperseg)
