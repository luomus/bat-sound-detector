from utils import wavfile
from utils.spectrogram import get_number_of_frames, get_magnitude_spectrum
from utils.misc import merge_ranges
from noise_removal import spectral_gating
from detection import median_clipping_detection
from config import DetectorConfig, DetectorConfigState

class Detector():
    def __init__(self, config):
        self.config = config

    def get_detections(self, file_path):
        data_length, fs = wavfile.get_data_length_and_sampling_rate(file_path)

        config = DetectorConfigState(self.config, fs, data_length)
        ranges = []

        for index in range(config.number_of_parts):
            fs, data, start_part = self._read_part(file_path, index, config)

            frequency, time, mag_spec = get_magnitude_spectrum(
                data, fs, config.nperseg, config.noverlap
            )

            if config.remove_noise:
                mag_spec = spectral_gating.remove_noise(
                    mag_spec,
                    config.nr_sensitivity,
                    config.nr_decrease,
                    config.nr_smoothing_time,
                    config.nr_smoothing_frequency
                )

            result = median_clipping_detection.detect(mag_spec, frequency, config)
            ranges += [[r[0] + start_part, r[1] + start_part] for r in result]

        ranges = merge_ranges(ranges)
        ranges = [self._get_data_range(r, config.nperseg, config.noverlap) for r in ranges]
        return ranges

    def _read_part(self, file_path, index, config):
        start_sample = index * (config.part_length - config.part_overlap)
        end_sample = start_sample + config.part_length - 1
        start_part = int(
            max(start_sample - config.nperseg, 0) /
            (config.nperseg - config.noverlap)
        )
        start_sample = start_part * (config.nperseg - config.noverlap)

        fs, data = wavfile.read(file_path, start_sample, end_sample)
        return fs, data, start_part

    def _get_data_range(self, spect_range, nperseg, noverlap):
        return [
            spect_range[0] * (nperseg - noverlap),
            spect_range[1] * (nperseg - noverlap) + nperseg
        ]
