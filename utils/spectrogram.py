import numpy as np

def get_number_of_frames(data_length, fs, nperseg, noverlap):
    return int(np.ceil((data_length - nperseg) / (nperseg - noverlap)) + 1)

def get_magnitude_spectrum(data, fs, nperseg, noverlap, normalize=True):
    frames, time = get_overlapping_frames(data, fs, nperseg, noverlap)
    spect = np.fft.rfft(frames * np.hamming(nperseg))
    mag_spec = np.abs(spect)
    if normalize:
        mag_spec = mag_spec / np.max(mag_spec)
    frequency = np.arange(int(np.floor(nperseg/2))+1) / nperseg * fs

    return frequency, time, mag_spec

def get_overlapping_frames(data, fs, nperseg, noverlap):
    number_of_frames = get_number_of_frames(len(data), fs, nperseg, noverlap)
    frames = np.zeros((number_of_frames, nperseg))

    for i in range(0, number_of_frames):
        frame_start = i * (nperseg - noverlap)
        frame_data = data[frame_start:frame_start + nperseg]
        frames[i, 0:len(frame_data)] = frame_data

    time = (nperseg / 2 + np.arange(number_of_frames) * (nperseg - noverlap)) / fs

    return frames, time
