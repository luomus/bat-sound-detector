import numpy as np

def vector_to_ranges(vector):
    changing_points = np.diff(np.pad((vector > 0).astype(int), (1, 1), 'constant'))
    starting_points = np.nonzero(changing_points == 1)[0]
    ending_points = np.nonzero(changing_points == -1)[0] - 1
    return np.column_stack((starting_points, ending_points))

def merge_ranges(ranges):
    if (len(ranges) < 2):
        return ranges

    ranges.sort(key=lambda r: r[0])

    start = ranges[0][0]
    end = ranges[0][1]
    result = []

    for r in ranges:
        if r[0] <= end:
            end = max(r[1], end)
        else:
            result.append([start, end])
            start = r[0]
            end = r[1]

    result.append([start, end])

    return result
