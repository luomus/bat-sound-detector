import numpy as np

def get_mask(mag_spec, f, config):
    column_mask = _get_column_mask(mag_spec, f, config)
    if config.mc_use_row_mask:
        row_mask = _get_row_mask(mag_spec, config)
        return np.logical_and(column_mask, row_mask).astype(np.uint8)

    return column_mask.astype(np.uint8)

def _get_row_mask(mag_spec, config):
    medians = np.median(mag_spec, axis=1)
    row_mask = mag_spec > config.mc_threshold * medians[:, None]
    return row_mask

def _get_column_mask(mag_spec, f, config):
    frames_in_supersegment = config.mc_segment_length
    threshold = config.mc_threshold
    interval = config.clock_interval
    min_f = config.min_frequency
    max_f = config.max_frequency

    segments, last_segment = _get_supersegments(mag_spec, frames_in_supersegment)
    column_mask = _get_basic_column_mask(segments, last_segment, threshold)

    if config.remove_clock_noise:
        column_mask2 = _get_column_mask_with_interval(segments, last_segment, threshold, interval, f, min_f, max_f)
        return np.logical_and(column_mask, column_mask2)

    return column_mask

def _get_basic_column_mask(segments, last_segment, threshold):
    medians = np.quantile(segments, [0.5], axis=1)[0]
    medians_last = np.quantile(last_segment, [0.5], axis=0)[0]

    column_mask = segments > threshold * medians[:, None, :]
    column_mask_last = last_segment > threshold * medians_last[None, :]

    column_mask = np.reshape(column_mask, (-1, np.shape(segments)[2]))
    return np.concatenate((column_mask, column_mask_last))

def _get_column_mask_with_interval(segments, last_segment, threshold, interval, f, min_f, max_f):
    column_mask = np.ones(np.shape(segments), dtype=bool)
    column_mask_last = np.ones(np.shape(last_segment), dtype=bool)

    frequency_mask = np.where(np.logical_and(f >= min_f, f < max_f))[0]
    f0 = frequency_mask[0]
    f1 = frequency_mask[-1] + 1

    for i in range(0, interval):
        frames = np.arange(i, np.shape(segments)[1], interval)
        frames_last = np.arange(i, len(last_segment), interval)
        medians = np.quantile(segments[:, frames, f0:f1], [0.5], axis=1)[0]
        medians_last = np.quantile(last_segment[frames_last, f0:f1], [0.5], axis=0)[0]

        interval_column_mask = segments[:, frames, f0:f1] > threshold * medians[:, None, :]
        interval_column_mask_last = last_segment[frames_last, f0:f1] > threshold * medians_last[None, :]

        column_mask[:, frames, f0:f1] = interval_column_mask
        column_mask_last[frames_last, f0:f1] = interval_column_mask_last

    column_mask = np.reshape(column_mask, (-1, np.shape(segments)[2]))
    return np.concatenate((column_mask, column_mask_last))

def _get_supersegments(mag_spec, frames_in_supersegment):
    in_last_segment = len(mag_spec) % frames_in_supersegment + frames_in_supersegment
    supersegments = np.reshape(mag_spec[:len(mag_spec)-in_last_segment], (-1, frames_in_supersegment, np.shape(mag_spec)[1]))
    return supersegments, mag_spec[len(mag_spec)-in_last_segment:]
