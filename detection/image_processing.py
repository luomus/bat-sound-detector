import numpy as np
import cv2

def filter_mask(mask, f, config):
    min_f = config.min_frequency
    max_f = config.max_frequency

    if config.remove_broadband_noise:
        min_certain_f = config.min_certain_frequency

        # reduce broadband noise
        half_min_f = int(min_f / 2)

        f_range1 = np.logical_and(f >= 0, f < half_min_f)
        f_range2 = np.logical_and(f >= half_min_f, f < min_f)
        f_range3 = np.logical_and(f >= min_certain_f - 1000, f < min_certain_f)

        threshold1 = round(np.sum(f_range1) * 0.025)
        threshold2 = round(np.sum(f_range2) * 0.025)
        threshold3 = round(np.sum(f_range3) * 0.1)

        sum1 = np.sum(mask[:, f_range1], axis=1)
        sum2 = np.sum(mask[:, f_range2], axis=1)
        sum3 = np.sum(mask[:, f_range3], axis=1)
        mask[np.logical_or(np.logical_and(sum1 >= threshold1, sum2 >= threshold2), sum3 >= threshold3)] = 0

    # keep only relevant frequency bands
    mask[:, np.logical_or(f < min_f, f >= max_f)] = 0
    return mask

def process_mask(mask):
    mask = cv2.GaussianBlur(mask, (3, 3), 0)
    mask = cv2.dilate(mask, np.ones((3, 5), np.uint8), iterations = 1)
    mask = cv2.erode(mask, np.ones((3, 5), np.uint8), iterations = 1)
    mask = cv2.medianBlur(mask, 3)
    return mask

def get_big_connected_components(mask, config):
    cc_with_stats = cv2.connectedComponentsWithStats(mask, 8, cv2.CV_32S)
    stats = cc_with_stats[2]

    big_components = np.logical_and(np.logical_and(
        stats[:, 4] > config.cc_area_theshold,
        stats[:, 3] > config.cc_width_threshold),
        stats[:, 2] > config.cc_height_threshold
    )
    big_components[0] = False
    return stats[big_components]

def connect_near_components(stats, config):
    call_number = np.arange(0, len(stats))
    wide_call_number = np.arange(0, len(stats))

    for i in range(0, len(stats)):
        left, width, top, height = stats[i][1], stats[i][3], stats[i][0], stats[i][2]

        for j in range(i+1, len(stats)):
            left2, width2, top2, height2 = stats[j][1], stats[j][3], stats[j][0], stats[j][2]
            x_diff = left2 - (left + width)

            if x_diff < config.nc_x_threshold_wide:
                if (
                    np.abs(top - top2) < config.nc_y_threshold_bottom and
                    np.abs((top+height) - (top2+height2)) < config.nc_y_threshold_top
                ):
                    if x_diff < config.nc_x_threshold:
                        call_number[call_number == call_number[j]] = i

                    wide_call_number[wide_call_number == wide_call_number[j]] = i
            else:
                break

    return call_number, wide_call_number
