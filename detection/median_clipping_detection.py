from detection import median_clipping, image_processing
from utils.misc import vector_to_ranges
from collections import Counter
import numpy as np

def detect(mag_spec, frequency, config):
    mask = median_clipping.get_mask(mag_spec, frequency, config)

    mask = image_processing.filter_mask(mask, frequency, config)
    mask = image_processing.process_mask(mask)

    stats = image_processing.get_big_connected_components(mask, config)
    call_number, wide_call_number = image_processing.connect_near_components(stats, config)

    result = _get_result_vector(call_number, wide_call_number, stats, len(mask), config)
    return vector_to_ranges(result)

def _get_result_vector(call_number, wide_call_number, stats, mag_spec_length, config):
    selected_indexes = []

    calls = Counter(call_number)
    for key in calls:
        if calls[key] >= config.min_number_of_calls:
            indexes = np.where(call_number == key)[0]
            if _get_number_of_calls(stats[indexes], config) >= config.min_number_of_calls:
                wide_key = wide_call_number[indexes][0]
                indexes = np.where(wide_call_number == wide_key)[0]
                selected_indexes += list(indexes)

    result = np.zeros(mag_spec_length, dtype=bool)
    for i in selected_indexes:
        left, width, top, height = stats[i][1], stats[i][3], stats[i][0], stats[i][2]
        result[
            int(np.max([left - config.padding, 0])):
            int(np.min([left + width + config.padding, len(result)]))
        ] = True

    return result

def _get_number_of_calls(stats, config):
    range_start = stats[0][1]
    range_end = stats[-1][1]

    has_call = np.zeros(range_end - range_start + 1)
    for s in stats:
        start = s[1] - range_start
        has_call[
            max(start-config.min_distance_between_pulses, 0):
            min(start+s[3]+config.min_distance_between_pulses, len(has_call))
        ] = 1

    return len(vector_to_ranges(has_call))
