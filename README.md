# Bat Sound Detector #

This code can be used to detect and extract bat sounds from audio recordings. Currently only supports wav-format.

Detection method is adapted from [the median clipping method](https://pdfs.semanticscholar.org/95e0/623c25126610014820dd7a04207a1fce900a.pdf).

### Short Instructions ###

1. Install Python 3 if not already installed

2. Install requirements with pip  
	```
	pip3 install -r requirements.txt
	```

3. Run the script for extracting bat calls  
	```
	python3 extract_calls.py <input_folder> <output_folder>
	```
