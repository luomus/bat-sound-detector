"""
Based on: https://timsainburg.com/noise-reduction-python.html

Modified to fit our needs

The MIT License (MIT)
Copyright (c) 2019, Tim Sainburg

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import numpy as np
import cv2

def remove_noise(
    mag_spec,
    sensitivity,
    decrease,
    smoothing_time=1,
    smoothing_frequecy=1
):
    # estimate noise statistics from low energy frames
    energies = np.sum(mag_spec, axis=1)
    threshold = np.quantile(energies, [0.1])[0]
    noise_mag_spec = mag_spec[energies < threshold, :]

    # noise statistics
    noise_mean = np.mean(noise_mag_spec, axis=0)
    noise_std = np.std(noise_mag_spec, axis=0)
    noise_thresh = noise_mean + noise_std * sensitivity

    mask_gain = np.min(mag_spec)

    # calculate smoothing filter to a mask
    smoothing_filter = np.outer(
        np.concatenate(
            [
                np.linspace(0, 1, smoothing_time + 1, endpoint=False),
                np.linspace(1, 0, smoothing_time + 2),
            ]
        )[1:-1],
        np.concatenate(
            [
                np.linspace(0, 1, smoothing_frequecy + 1, endpoint=False),
                np.linspace(1, 0, smoothing_frequecy + 2),
            ]
        )[1:-1],
    )
    smoothing_filter = smoothing_filter / np.sum(smoothing_filter)

    # calculate mask
    threshold = np.repeat(
        np.reshape(noise_thresh, [1, len(noise_mean)]),
        np.shape(mag_spec)[0],
        axis=0
    )
    mask = mag_spec < threshold

    # convolve the mask with a smoothing filter
    mask = cv2.filter2D(mask.astype(np.float32), -1, smoothing_filter[::-1,::-1].astype(np.float32), borderType=cv2.BORDER_CONSTANT)
    mask = mask * decrease

    # mask the signal
    mag_spec_masked = (
        mag_spec * (1 - mask)
        + np.ones(np.shape(mask_gain)) * mask_gain * mask
    )

    return mag_spec_masked
